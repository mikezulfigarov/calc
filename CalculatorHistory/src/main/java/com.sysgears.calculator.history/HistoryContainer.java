package com.sysgears.calculator.history;


public interface HistoryContainer<T> {

    /**
     * Adds element to the container.
     *
     * @param elem element to be added
     * @return whether the addition succeeded
     */
    boolean add(T elem);

    /**
     * Removes element from the container.
     *
     * @param o element to be added
     * @return whether the deletion succeeded
     */
    boolean remove(Object o);

    /**
     * Clears container.
     */
    void clear();

    /**
     * Returns <b>true</b> if empty, <b>false</b> otherwise.
     *
     * @return <b>true</b> if empty, <b>false</b> otherwise.
     */
    boolean isEmpty();

    /**
     * Returns an array of unique elements.
     *
     * @return an array of unique elements.
     */
    T[] getUnique();

    /**
     * Returns all elements in the container as a array.
     *
     * @return all elements in the container as a array.
     */
    T[] getAll();


}
