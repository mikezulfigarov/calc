package com.sysgears.calculator.history;

import java.util.Collection;

/**
 * Abstract history collection interface.
 * You can implement it for example to work with file or database, or a
 * collection e.t.c.
 *
 * @param <T> history item.
 */
public interface HistoryCollectionContainer<T>
        extends HistoryContainer<T>, Collection<T> {
}
