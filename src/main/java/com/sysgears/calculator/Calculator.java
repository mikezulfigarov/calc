package com.sysgears.calculator;

import com.sysgears.calculator.logic.Parser;
import com.sysgears.calculator.logic.history.ExpressionHistoryCollectionContainer;
import com.sysgears.calculator.logic.HistoryEvaluator;
import com.sysgears.calculator.logic.ScalableParser;
import com.sysgears.calculator.logic.exceptions.ExpressionSyntaxError;
import com.sysgears.calculator.logic.operations.*;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import java.util.*;

public class Calculator {

    public static boolean endSession = false;

    private static void printlnHistoryQueryResult(
            HistoryEvaluator.ExpressionHistoryItem[] items) {

        if (items.length > 0)
            for (HistoryEvaluator.ExpressionHistoryItem item : items)
                System.out.println(item);
        else
            System.out.println("Your history is empty");
    }

    private static Logger logger = Logger.getLogger(Calculator.class);

    public static void main(String[] args) {

        try {
            logger.info("NEW USER SESSION STARTED");

            Parser parser = new ScalableParser.Builder()
                    .registerBinaryOperator("^", new Power())
                    .registerUnaryOperator("-", new UnaryMinus())
                    .registerUnaryOperator("sqrt", new UnaryOperation() {
                        @Override
                        public Number apply(Number operand) {
                            return Math.sqrt(operand.doubleValue());
                        }
                    })

                    .registerUnaryOperator("cos", new UnaryOperation() {
                        @Override
                        public Number apply(Number operand) {
                            return Math.cos(operand.doubleValue());
                        }
                    })

                    .registerUnaryOperator("^", new UnaryOperation() {
                        @Override
                        public Number apply(Number operand) {
                            return Math.pow(operand.doubleValue(), 2);
                        }
                    })

/*                    .registerUnaryOperator("s", new UnaryOperation() {
                        @Override
                        public Number apply(Number operand) {
                            return null;
                        }
                    })*/

                    .build();

            String input;

            Scanner in = new Scanner(System.in);

            ExpressionHistoryCollectionContainer expressionHistory
                    = new ExpressionHistoryCollectionContainer();

            HistoryEvaluator historyEvaluator = new HistoryEvaluator(parser,
                    expressionHistory);

            while (!endSession) {
                try {
                    System.out.print("Please enter your expression " +
                            "(or type ':q' to quit):\n> ");
                    input = in.nextLine();

                    System.out.println();

                    if (input.equals(":q")) break;

                    else if (input.equals("unique"))
                        printlnHistoryQueryResult(historyEvaluator
                                .getUniqueFromHistory());

                    else if (input.equals("history"))
                        printlnHistoryQueryResult(historyEvaluator
                                .getAllHistory());
                    else {

                        double result = historyEvaluator.evaluate(input);

                        logger.info(String.format("Evaluation result %s",
                                result));

                        System.out.printf("%s = %s \n", input, result);
                    }

                } catch (NoSuchElementException ex) {
                } catch (ExpressionSyntaxError ex) {
                    logger.info(ex.getMessage().replaceAll("\\n", ". "));
                    System.err.println(ex.getMessage());
                } catch (Exception ex) {
                    logger.error("An error occured:", ex);
                }
            }
        } finally {
            MDC.put("expression", "");
            logger.info("USER SESSION ENDED");
        }


    }
}

