package com.sysgears.calculator.logic.validators;

import com.sysgears.calculator.logic.Parser;
import com.sysgears.calculator.logic.ScalableParser;

/**
 * Defines validation criteria for {@link ScalableParser} used to validate the
 * operator registered to {@link ScalableParser} instance.
 */
public class OperatorValidator extends StringValidator {

    /**
     * All criteria is defined here. Takes {@link ScalableParser} instance to get
     * regexes to be used in criterions.
     *
     * @param parser {@link ScalableParser} instance, due to which implementation
     *                             the criteria is built.
     */
    public OperatorValidator(final Parser parser) {

        addCriterions(

                new Criterion() {

                    public String check(String str) {
                        return (str.length() == 0) ? "You can't register " +
                                "operator with zero-length signature" : null;
                    }

                },

                new Criterion() {

                    public String check(String str) {

                        for (String op : parser.getAllOperatorsSignatures())
                            if (op.contains(str) && (op.length()
                                    != str.length()))
                                return "You can't use symbols, contained in " +
                                        "already defined operators, except " +
                                        "for overriding.";

                        return null;
                    }

                },

                new RegexCriterion(parser.getAnyOperatorRegex() + "{2,}",
                        "You're not allowed to define operators that " +
                                "contains or completely combined from " +
                                "already defined, except for " +
                                "overriding.", false),

                new RegexCriterion("(\\(|\\)|\\[|\\]|\\.|\\s)+", "You're not " +
                        "allowed to define operators that contains following " +
                        "special symbols ['(', ')', '[', ']', ' ', '.']", false)
        );

    }


}
