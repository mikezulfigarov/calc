package com.sysgears.calculator.logic.history;

import com.sysgears.calculator.history.HistoryCollectionContainer;
import com.sysgears.calculator.logic.HistoryEvaluator;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * {@link HistoryCollectionContainer}
 * for {@link HistoryEvaluator.ExpressionHistoryItem}
 */
public class ExpressionHistoryCollectionContainer
        implements
        HistoryCollectionContainer<HistoryEvaluator.ExpressionHistoryItem> {

    /**
     * {@link Logger} instance, which is used to log debug information into
     * calculator.log
     */
    private static Logger logger = Logger.getLogger(ExpressionHistoryCollectionContainer.class);

    /**
     * {@link HistoryEvaluator.ExpressionHistoryItem} stack
     */
    private Stack<HistoryEvaluator.ExpressionHistoryItem> container
            = new Stack<HistoryEvaluator.ExpressionHistoryItem>();

    /**
     * Returns {@link Set} of unique
     * {@link HistoryEvaluator.ExpressionHistoryItem}
     *
     * @return {@link Set} of unique
     * {@link HistoryEvaluator.ExpressionHistoryItem}
     */
    public HistoryEvaluator.ExpressionHistoryItem[] getUnique() {

        Set<HistoryEvaluator.ExpressionHistoryItem> uniqueElementsSet
                = new LinkedHashSet<HistoryEvaluator
                .ExpressionHistoryItem>(container);

        return uniqueElementsSet
                .toArray(new HistoryEvaluator
                        .ExpressionHistoryItem[uniqueElementsSet.size()]);
    }

    /**
     * Returns an array of all
     * {@link HistoryEvaluator.ExpressionHistoryItem} in container
     *
     * @return array of all
     * {@link HistoryEvaluator.ExpressionHistoryItem} in container
     */
    public HistoryEvaluator.ExpressionHistoryItem[] getAll() {
        return container.toArray(new HistoryEvaluator
                .ExpressionHistoryItem[container.size()]);
    }

    /**
     * Returns container size
     *
     * @return container size
     */
    public int size() {
        return container.size();
    }

    /**
     * Returns <b>true</b> if empty, <b>false</b> otherwise.
     *
     * @return <b>true</b> if empty, <b>false</b> otherwise.
     */
    public boolean isEmpty() {
        return container.isEmpty();
    }

    /**
     * Returns <b>true</b> if container instance contains {@code obj},
     * <b>false</b> otherwise.
     *
     * @param obj object searched in container
     * @return <b>true</b> if container instance contains {@code obj},
     * <b>false</b> otherwise.
     */
    public boolean contains(Object obj) {
        return container.contains(obj);
    }

    /**
     * Returns {@link Iterator} over container.
     *
     * @return @link Iterator} over container.
     */
    public Iterator<HistoryEvaluator.ExpressionHistoryItem> iterator() {
        return container.iterator();
    }

    /**
     * Converts container to an array
     *
     * @return array of container elements.
     */
    public Object[] toArray() {
        return container.toArray();
    }

    /**
     * Generic version of {@link ExpressionHistoryCollectionContainer#toArray()}
     *
     * @return array of container elements.
     */
    public <T> T[] toArray(T[] ts) {
        return container.toArray(ts);
    }

    /**
     * Adds element to the container.
     *
     * @param s element to be added
     * @return whether the addition succeeded
     */
    public boolean add(HistoryEvaluator.ExpressionHistoryItem s) {

        return container.add(s);
    }

    /**
     * Removes element from the container.
     *
     * @param o element to be added
     * @return whether the deletion succeeded
     */
    public boolean remove(Object o) {
        return container.remove(o);
    }

    /**
     * Returns <b>true</b> if container instance contains all elements in
     * {@code collection},
     * <b>false</b> otherwise.
     *
     * @param collection elements to be checked
     * @return <b>true</b> if container instance contains all elements in
     * {@code collection},
     * <b>false</b> otherwise.
     */
    public boolean containsAll(Collection<?> collection) {
        return container.containsAll(collection);
    }

    /**
     * Adds all elements from {@code collection} to the container.
     *
     * @param collection element to be added.
     * @return whether the addition succeeded.
     */
    public boolean addAll(Collection<? extends
            HistoryEvaluator.ExpressionHistoryItem> collection) {
        return container.addAll(collection);
    }

    /**
     * Removes all elements contained in {@code collection}
     * from the container.
     *
     * @param collection elements to be removed.
     * @return whether the deletion succeeded.
     */
    public boolean removeAll(Collection<?> collection) {
        return container.removeAll(collection);
    }

    /**
     * Retains only the elements in this collection that are contained in the
     * specified collection.
     *
     * @param collection elements to be retained.
     * @return whether the retaining succeeded.
     */
    public boolean retainAll(Collection<?> collection) {
        return container.retainAll(collection);
    }

    /**
     * Clears container.
     */
    public void clear() {
        container.clear();
    }
}
