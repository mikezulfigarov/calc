package com.sysgears.calculator.logic.operations;

/**
 * Represents multiplication operation logic
 */
public class Multiplication extends BinaryOperation {

    /**
     * {@link Multiplication} default constructor
     */
    public Multiplication() {

        super(0, 0);
    }

    /**
     * {@link Multiplication} ctor which sets it's state
     *
     * @param leftOp  value for {@link Multiplication#leftOp} - left operand
     * @param rightOp value for {@link Multiplication#rightOp} - right operand
     */
    public Multiplication(final Number leftOp, final Number rightOp) {

        super(leftOp, rightOp);
    }

    /**
     * Applies multiplication operation to the given {@code leftOp}
     * and {@code rightOp} operands
     *
     * @param leftOp  left operand
     * @param rightOp right operand
     * @return a result of a multiplication of the {@code leftOp}
     * and {@code rightOp} operands
     */
    @Override
    public Number apply(final Number leftOp, final Number rightOp) {

        return leftOp.doubleValue() * rightOp.doubleValue();
    }

    /**
     * Returns {@link Associativity} type of this operation
     * ( {@link Associativity#LEFT} by default)
     *
     * @return associativity type of this operation
     * ( {@link Associativity#LEFT} by default)
     */
    @Override
    public Associativity getAssociativity() {

        return Associativity.LEFT;
    }

    /**
     * Returns {@link Precedence} of this operation
     * ( {@link Precedence#HIGH} by deafult)
     *
     * @return {@link Precedence} of this operation
     * ( {@link Precedence#HIGH} by deafult)
     */
    @Override
    public Precedence getPrecedence() {

        return Precedence.HIGH;
    }
}
