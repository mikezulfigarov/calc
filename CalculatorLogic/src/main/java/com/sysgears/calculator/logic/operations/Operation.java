package com.sysgears.calculator.logic.operations;

/**
 * Abstract class encapsulating simple operation logic
 */
public abstract class Operation extends Number implements Comparable<Operation> {

    /**
     * Precedence levels
     */
    public enum Precedence {
        /**
         * Low precedence - e.g. sum, deduction
         */
        LOW,
        /**
         * High precedence - e.g. multiplication, division
         */
        HIGH,
        /**
         * Highest operator precedence - the highest - e.g. unary minus
         * or power operation
         */
        HIGHEST
    }

    /**
     * Associativuty type of the operation
     */
    public enum Associativity {
        /**
         * Left associative operation
         */
        LEFT,
        /**
         * Right associative operation
         */
        RIGHT
    }

    /**
     * You should implement this in order to apply operation to the operands
     *
     * @return operation result
     */
    public abstract Number apply();

    /**
     * Compares two operation by {@link BinaryOperation.Precedence}
     *
     * @param operation {@link Operation} being comapared with this one
     * @return <b>(result > 0)</b> if this {@link Operation} has higher
     * {@link BinaryOperation.Precedence}.<br/>
     * <b>(result == 0) </b> if equal.<br/>
     * <b>(result < 0) </b> if lower.<br/>
     */
    public int compareTo(Operation operation) {

        return this.getPrecedence().ordinal() -
                operation.getPrecedence().ordinal();
    }

    /**
     * Each concrete operation has it's own associativity type thus you should
     * provide concrete implementation for each
     *
     * @return {@link BinaryOperation.Associativity} of operation
     */
    public abstract BinaryOperation.Associativity getAssociativity();

    /**
     * Each concrete operation has it's own precedence level thus you should
     * provide concrete implementation for each
     *
     * @return {@link BinaryOperation.Precedence} of the operation
     */
    public abstract BinaryOperation.Precedence getPrecedence();

    /**
     * Returns {@link Operation} result as an integer value
     *
     * @return {@link Operation} result as an integer value
     */
    @Override
    public int intValue() {

        return apply().intValue();
    }

    /**
     * Returns {@link Operation} result as a long value
     *
     * @return {@link Operation} result as a long value
     */
    @Override
    public long longValue() {

        return apply().longValue();
    }

    /**
     * Returns {@link Operation} result as a float value
     *
     * @return {@link Operation} result as a float value
     */
    @Override
    public float floatValue() {

        return apply().floatValue();
    }

    /**
     * Returns {@link Operation} result as a double value
     *
     * @return {@link Operation} result as a double value
     */
    @Override
    public double doubleValue() {

        return apply().doubleValue();
    }

    /**
     * Returns {@link Operation} result as a string value
     *
     * @return {@link Operation} result as a string value
     */
    @Override
    public String toString() {

        return apply().toString();
    }
}
