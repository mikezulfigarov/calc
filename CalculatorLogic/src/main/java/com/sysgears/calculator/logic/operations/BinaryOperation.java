package com.sysgears.calculator.logic.operations;

import java.lang.reflect.InvocationTargetException;

/**
 * Abstract class encapsulating simple numeric binary operation logic
 */
public abstract class BinaryOperation extends Operation {

    /**
     * Left operand of this {@link BinaryOperation}
     */
    public final Number leftOp;

    /**
     * Right operand of this {@link BinaryOperation}
     */
    public final Number rightOp;

    /**
     * Default {@link BinaryOperation} constructor, which specifies it's state
     */
    public BinaryOperation() {

        this(0, 0);
    }

    /**
     * {@link BinaryOperation} constructor, which specifies it's state
     *
     * @param leftOp  left operand of the {@link BinaryOperation}
     * @param rightOp right operand of the {@link BinaryOperation}
     */
    public BinaryOperation(final Number leftOp, final Number rightOp) {

        this.leftOp = leftOp;
        this.rightOp = rightOp;
    }

    /**
     * Applies {@link BinaryOperation} logic to the
     * {@link BinaryOperation#leftOp}
     * and {@link BinaryOperation#rightOp}
     *
     * @return result of the {@link BinaryOperation}
     */
    @Override
    public Number apply() {

        return apply(leftOp, rightOp);
    }

    /**
     * Returns {@link Associativity} type of this operation
     * ( {@link Associativity#LEFT} by default)
     *
     * @return associativity type of this operation
     * ( {@link Associativity#LEFT} by default)
     */
    //@Override
    public Associativity getAssociativity() {

        return Associativity.LEFT;
    }

    /**
     * Returns {@link Precedence} of this operation
     * ( {@link Precedence#LOW} by deafult)
     *
     * @return {@link Precedence} of this operation
     * ( {@link Precedence#LOW} by deafult)
     */
    @Override
    public Precedence getPrecedence() {

        return Precedence.LOW;
    }

    /**
     * Applies {@link BinaryOperation} logic to the operands
     * passed as a parameters
     *
     * @param leftOp  left operand
     * @param rightOp right operand
     * @return Result of the {@link BinaryOperation} execution
     */
    public abstract Number apply(final Number leftOp, final Number rightOp);

    /**
     * Simple {@link BinaryOperation} builder. Due to it's immutability the
     * {@link BinaryOperation} class needs one
     */
    public static class Builder {
        private Number leftOp = 0;
        private Number rightOp = 0;

        /**
         * Default {@link BinaryOperation.Builder} builder ctor
         */
        public Builder() {

            this(0, 0);
        }

        /**
         * {@link BinaryOperation.Builder}
         * ctor that provides default values for the
         * {@link BinaryOperation.Builder#leftOp} and
         * {@link BinaryOperation.Builder#rightOp}. They can still be set via
         * the appropriate setters though.
         *
         * @param leftOp  value for {@link BinaryOperation.Builder#leftOp}
         * @param rightOp value for {@link BinaryOperation.Builder#rightOp}
         */
        public Builder(Number leftOp, Number rightOp) {

            this.leftOp = leftOp;
            this.rightOp = rightOp;
        }

        /**
         * {@link BinaryOperation.Builder#leftOp} setter method
         *
         * @param leftOp value for {@link BinaryOperation.Builder#leftOp}
         * @return current {@link BinaryOperation.Builder} instance
         */
        public Builder setLeftOp(final Number leftOp) {

            this.leftOp = leftOp;
            return this;
        }

        /**
         * {@link BinaryOperation.Builder#rightOp} setter method
         *
         * @param rightOp value for {@link BinaryOperation.Builder#leftOp}
         * @return current {@link BinaryOperation.Builder} instance
         */
        public Builder setRightOp(final Number rightOp) {

            this.rightOp = rightOp;
            return this;
        }

        /**
         * Returns new instance of {@code whichOne.class} set with this
         * {@link BinaryOperation.Builder}
         *
         * @param whichOne class parameter to specify which class the
         *                 builder should instantiate
         * @return new instance of {@link BinaryOperation} descendant,
         * specifically {@code whichOne.class}
         * @throws NoSuchMethodException     if no appropriate ctor was found
         * @throws IllegalAccessException    if such ctor is out of reach
         *                                   by access
         * @throws InvocationTargetException if there were some errors
         *                                   during the ctor invocation
         * @throws InstantiationException    if a class is abstract
         */
        public BinaryOperation build(Class<? extends BinaryOperation> whichOne)
                throws NoSuchMethodException, IllegalAccessException,
                InvocationTargetException, InstantiationException {

            return whichOne.getConstructor(Number.class, Number.class)
                    .newInstance(leftOp, rightOp);
        }


    }

}
