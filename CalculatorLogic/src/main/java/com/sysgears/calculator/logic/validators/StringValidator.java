package com.sysgears.calculator.logic.validators;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Immutable string validator. Performs validation of strings, based on
 * registered criteria, presented by {@link StringValidator.Criterion}
 * implementations. Could be created either with {@link StringValidator.Builder}
 * or by deriving from this class and calling protected
 * {@link StringValidator#addCriterions(Criterion...)}) from your ctor.
 */
public class StringValidator {

    /**
     * Represents validation criterion
     */
    public interface Criterion {
        /**
         * Checks the string by some criterion and returns a string
         * (maybe some validation message). For example:
         * <pre>
         * {@code new Criterion() {
         *
         *      public String check(String str) {
         *          return (str.length() == 0)
         *          ? "You can't use zero-length string"
         *          : null;
         *      }
         *
         *  }
         * }
         * </pre>
         * @param str a string to be checked
         * @return
         */
        String check(final String str);
    }

    /**
     * Creates an immutable {@link StringValidator } instance.
     */
    public static class Builder {

        /**
         * Instance to be built
         */
        private StringValidator instance = null;

        /**
         * Performs lazy initialization of the
         * {@link StringValidator.Builder#instance}
         */
        private void lazyInit() {
            if (instance == null) instance = new StringValidator();
        }

        /**
         * Registers validations {@link StringValidator.Criterion}s for
         * {@link StringValidator} instance to be built.
         *
         * @param criterions one or more {@link StringValidator.Criterion}s to
         *                   be registered in the upcoming {
         *                   @link StringValidator} instance.
         * @return {@link StringValidator.Builder} instance to allow chain
         * method calls
         */
        public Builder registerCriterions(final Criterion... criterions) {
            lazyInit();
            instance.addCriterions(criterions);
            return this;
        }

        /**
         * Returns configured {@link StringValidator} instance.
         * If no configuration calls
         * to {@link StringValidator.Builder#addCriterions(Criterion...)}
         * preceded then useless instance of {@link StringValidator} is
         * returned. You were warned =).
         *
         * @return new configured (if was) or default
         * (if wasn't) {@link StringValidator} instance.
         */
        public StringValidator build() {
            lazyInit();
            StringValidator holder = instance;
            instance = null;

            return holder;
        }
    }

    /**
     * Criteria to be checked by {@link StringValidator}.
     */
    private ArrayList<Criterion> criteria = new ArrayList<Criterion>();

    /**
     * Default ctor. To avoid accidental creating of useless validators made
     * protected.
     */
    protected StringValidator() {
    }

    /**
     * Performs validation and returns all messages as an array of Strings
     *
     * @param str a string to be validated
     * @return all messages returned during validation as an array of Strings
     */
    public String[] getErrorMessages(final String str) {

        ArrayList<String> tmp = new ArrayList<String>();

        String msg;

        for (Criterion c : criteria) {
            msg = c.check(str);
            if (msg != null) {
                tmp.add(msg);
            }
        }
        String[] result = new String[tmp.size()];
        tmp.toArray(result);

        return result;
    }

    /**
     * Same as {@link StringValidator#getErrorMessages(String)} but returns all messages
     * as a single string, separated with '\n' characters.
     *
     * @param str a string to be validated
     * @return all messages returned during validation as an array of Strings
     */
    public String getErrorMessagesInOneString(final String str) {

        String[] errors = getErrorMessages(str);

        if (errors.length == 0)
            return null;

        StringBuilder resultMsg = new StringBuilder();

        for (String msg : errors)
            resultMsg.append(msg).append("\n");

        return resultMsg.toString();
    }

    protected void addCriterions(final Criterion... criterions) {
        Collections.addAll(criteria, criterions);
    }

}
