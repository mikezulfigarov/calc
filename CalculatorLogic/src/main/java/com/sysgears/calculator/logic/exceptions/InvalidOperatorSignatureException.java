package com.sysgears.calculator.logic.exceptions;

/**
 * Unchecked exception.
 * Thrown when there has been a try to define operation with malformed
 * signature.
 */
public class InvalidOperatorSignatureException extends RuntimeException {
    /**
     * Constructs exception object with the specified {@code message}
     *
     * @param message string, that contains exception details
     */
    public InvalidOperatorSignatureException(String message) {

        super(message);
    }

    /**
     * Constructs exception cased by {@code cause} instance.
     *
     * @param cause
     */
    public  InvalidOperatorSignatureException(Throwable cause) {

        super(cause);
    }

}
