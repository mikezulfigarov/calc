package com.sysgears.calculator.logic.operations;

/**
 * Represents division operation logic
 */
public class Division extends BinaryOperation {

    /**
     * {@link Division} default constructor
     */
    public Division() {

        super(0, 0);
    }

    /**
     * {@link Division} constructor, which specifies it's state
     *
     * @param leftOp  left operand of the {@link Division}
     * @param rightOp right operand of the {@link Division}
     */
    public Division(Number leftOp, Number rightOp) {

        super(leftOp, rightOp);
    }

    /**
     * Applies division operation to the given {@code leftOp}
     * and {@code rightOp} operands
     *
     * @param leftOp  left operand
     * @param rightOp right operand
     * @return a result of a division of the {@code leftOp}
     * and {@code rightOp} operands
     */
    @Override
    public Number apply(Number leftOp, Number rightOp) {

        return leftOp.doubleValue() / rightOp.doubleValue();
    }

    /**
     * Returns {@link Associativity} type of this operation
     * ( {@link Associativity#LEFT} by default)
     *
     * @return associativity type of this operation
     * ( {@link Associativity#LEFT} by default)
     */
    @Override
    public Associativity getAssociativity() {

        return Associativity.LEFT;
    }

    /**
     * Returns {@link Precedence} of this operation
     * ( {@link Precedence#HIGH} by deafult)
     *
     * @return {@link Precedence} of this operation
     * ( {@link Precedence#HIGH} by deafult)
     */
    @Override
    public Precedence getPrecedence() {

        return Precedence.HIGH;
    }
}
