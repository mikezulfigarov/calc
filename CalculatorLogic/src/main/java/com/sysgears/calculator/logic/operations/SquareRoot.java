package com.sysgears.calculator.logic.operations;

/**
 * Represents square root operation
 */
public class SquareRoot extends UnaryOperation {

    /**
     * {@link SquareRoot} default constructor.
     */
    public SquareRoot() {

        super(0);
    }

    /**
     * {@link SquareRoot} constructor. Takes a numeric value a negation of
     * which is expected as a result of the operation
     *
     * @param operand a numeric value a negation of
     *                which is expected as a result
     */
    public SquareRoot(Number operand) {

        super(operand);
    }

    /**
     * Applies a square root operation to the numeric value passed as a
     * parameter
     *
     * @param operand a numeric value a square root of
     *                which is expected as a result
     * @return square root value of the {@code operand}
     */
    @Override
    public Number apply(Number operand) {
        return Math.sqrt(operand.doubleValue());
    }
}
