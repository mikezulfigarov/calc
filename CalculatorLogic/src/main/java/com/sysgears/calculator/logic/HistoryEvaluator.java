package com.sysgears.calculator.logic;

import com.sysgears.calculator.logic.exceptions.ExpressionSyntaxError;
import com.sysgears.calculator.history.HistoryCollectionContainer;

import java.util.Date;

/**
 * Parses and evaulates string expression and  stores it in associated
 * {@link HistoryCollectionContainer}
 */
public class HistoryEvaluator extends Evaluator {


    /**
     * Expression wrapper used to store it in history.
     */
    public static class ExpressionHistoryItem {

        /**
         * Expression string without spaces.
         */
        private String expression;

        /**
         * Expression result.
         */
        private String result;

        /**
         * {@link Date} object containing info about whe was the
         * expression evaluated.
         */
        private Date date;

        /**
         * Creates {@link HistoryEvaluator.ExpressionHistoryItem} instance
         * with custom date and evaluation result.
         *
         * @param expression expression string.
         * @param result     expression evaluation result.
         */
        public ExpressionHistoryItem(String expression, String result, Date date) {

            this.expression = expression.replaceAll("\\s+", "");
            this.result = result;
            this.date = date;
        }

        /**
         * Creates {@link HistoryEvaluator.ExpressionHistoryItem} instance.
         *
         * @param expression expression string.
         * @param result     expression evaluation result.
         */
        public ExpressionHistoryItem(String expression, String result) {

            this(expression, result, new Date());
        }


        /**
         * Returns expression string.
         *
         * @return expression string.
         */
        public String getExpression() {
            return expression;
        }

        /**
         * Returns exspression result.
         *
         * @return exspression result.
         */
        public String getResult() {
            return result;
        }

        /**
         * Returns info about whe was the
         * expression evaluated.
         *
         * @return info about whe was the
         * expression evaluated.
         */
        public Date getDate() {
            return date;
        }

        /**
         * Determines whether the
         * {@link HistoryEvaluator.ExpressionHistoryItem}s are equal.
         *
         * @param obj an object to compare with
         * @return <b>true</b> if {@code this} and {@code obj} are equal, <br/>
         * <b>false</b> - otherwise.
         */
        @Override
        public boolean equals(Object obj) {

            if (obj instanceof ExpressionHistoryItem) {
                ExpressionHistoryItem that = ((ExpressionHistoryItem) obj);

                String thatExpression = that.expression;

                return this.expression.equals(thatExpression);
            } else

                return false;
        }

        /**
         * Returns expression hashCode.
         *
         * @return expression hashCode.
         */
        @Override
        public int hashCode() {
            return expression.hashCode();
        }

        /**
         * Returns string representation of
         * {@link HistoryEvaluator.ExpressionHistoryItem} in format:
         * ("[expression] = [result] : [date]").
         *
         * @return string representation of
         * {@link HistoryEvaluator.ExpressionHistoryItem} in format:
         * ("[expression] = [result] : [date]").
         */
        @Override
        public String toString() {
            return String.format("%s = %s : %s", expression, result, date);
        }
    }

    /**
     * Contains {@link HistoryEvaluator.ExpressionHistoryItem}s and performs
     * basic operations on them.
     */
    private HistoryCollectionContainer<ExpressionHistoryItem> container;

    /**
     * Creates {@link HistoryEvaluator} instance. {@link ScalableParser} instance is
     * used to parse the expression and {@link HistoryCollectionContainer} instance is
     * used to handle history saving and fetching.
     *
     * @param parser    {@link ScalableParser} instance which is
     *                  used to parse the expression
     * @param container {@link HistoryCollectionContainer} instance which is
     *                  used to handle history saving and fetching.
     */
    public HistoryEvaluator(Parser parser,
                            HistoryCollectionContainer<ExpressionHistoryItem> container) {
        super(parser);
        this.container = container;
    }

    /**
     * Evaluates given expression and stores it in
     * {@link HistoryEvaluator#container}.
     *
     * @param expression expression to be evaluated.
     * @return expression value.
     * @throws ExpressionSyntaxError f there were one or more syntax errors
     *                               made in expression.
     */
    @Override
    public double evaluate(String expression) throws ExpressionSyntaxError {

        double result = super.evaluate(expression);

        container.add(new ExpressionHistoryItem(expression, Double
                .toString(result), new Date()));

        return result;
    }

    /**
     * Returns an array of all
     * {@link HistoryEvaluator.ExpressionHistoryItem} in container
     *
     * @return array of all
     * {@link HistoryEvaluator.ExpressionHistoryItem} in container
     */
    public ExpressionHistoryItem[] getAllHistory() {
        return container.getAll();
    }

    /**
     * Returns an array of unique
     * {@link HistoryEvaluator.ExpressionHistoryItem}
     *
     * @return array of unique
     * {@link HistoryEvaluator.ExpressionHistoryItem}
     */
    public ExpressionHistoryItem[] getUniqueFromHistory() {
        return container.getUnique();
    }

}
