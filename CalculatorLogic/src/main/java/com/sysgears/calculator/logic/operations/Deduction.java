package com.sysgears.calculator.logic.operations;

/**
 * Represents deduction operation
 */
public class Deduction extends BinaryOperation {

    /**
     * {@link Deduction} default constructor
     */
    public Deduction() {

        super(0, 0);
    }

    /**
     * {@link Deduction} constructor, which specifies it's state
     *
     * @param leftOp  left operand of the {@link Deduction}
     * @param rightOp right operand of the {@link Deduction}
     */
    public Deduction(Number leftOp, Number rightOp) {

        super(leftOp, rightOp);
    }

    /**
     * Applies deduction operation to the given {@code leftOp}
     * and {@code rightOp} operands
     *
     * @param leftOp  left operand
     * @param rightOp right operand
     * @return a result of a deduction of the {@code leftOp}
     * and {@code rightOp} operands
     */
    @Override
    public Number apply(Number leftOp, Number rightOp) {

        return leftOp.doubleValue() - rightOp.doubleValue();
    }

    /**
     * Returns {@link Associativity} type of this operation
     * ( {@link Associativity#LEFT} by default)
     *
     * @return associativity type of this operation
     * ( {@link Associativity#LEFT} by default)
     */
    @Override
    public Associativity getAssociativity() {

        return Associativity.LEFT;
    }

    /**
     * Returns {@link Precedence} of this operation
     * ( {@link Precedence#LOW} by deafult)
     *
     * @return {@link Precedence} of this operation
     * ( {@link Precedence#LOW} by deafult)
     */
    @Override
    public Precedence getPrecedence() {

        return Precedence.LOW;
    }
}
