package com.sysgears.calculator.logic.operations;

/**
 * Represents summation operation logic
 */
public class Sum extends BinaryOperation {

    /**
     * {@link Sum} default constructor
     */
    public Sum() {

        super(0, 0);
    }

    /**
     * {@link Multiplication} ctor which sets it's state
     *
     * @param leftOp  value for {@link Sum#leftOp} - left operand
     * @param rightOp value for {@link Sum#rightOp} - right operand
     */
    public Sum(final Number leftOp, final Number rightOp) {

        super(leftOp, rightOp);
    }

    /**
     * Applies summation operation to the given {@code leftOp}
     * and {@code rightOp} operands
     *
     * @param leftOp  left operand
     * @param rightOp right operand
     * @return a result of a summation of the {@code leftOp}
     * and {@code rightOp} operands
     */
    @Override
    public Number apply(Number leftOp, Number rightOp) {

        return leftOp.doubleValue() + rightOp.doubleValue();
    }

    /**
     * Returns {@link Associativity} type of this operation
     * ( {@link Associativity#LEFT} by default)
     *
     * @return associativity type of this operation
     * ( {@link Associativity#LEFT} by default)
     */
    @Override
    public Associativity getAssociativity() {

        return Associativity.LEFT;
    }

    /**
     * Returns {@link Precedence} of this operation
     * ( {@link Precedence#LOW} by deafult)
     *
     * @return {@link Precedence} of this operation
     * ( {@link Precedence#LOW} by deafult)
     */
    @Override
    public Precedence getPrecedence() {

        return Precedence.LOW;
    }
}
