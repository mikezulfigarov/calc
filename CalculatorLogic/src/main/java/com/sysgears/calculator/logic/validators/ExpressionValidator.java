package com.sysgears.calculator.logic.validators;

import com.sysgears.calculator.logic.Parser;
import com.sysgears.calculator.logic.ScalableParser;

/**
 * Defines validation criteria for {@link ScalableParser} used to validate the
 * expression passed to {@link ScalableParser} instance.
 */
public class ExpressionValidator extends StringValidator {

    /**
     * All criteria is defined here. Takes {@link ScalableParser} instance to get
     * regexes to be used in criterions.
     *
     * @param parser {@link ScalableParser} instance, due to which implementation
     *               the criteria is built.
     */
    public ExpressionValidator(final Parser parser) {

        addCriterions(
                new RegexCriterion("(\\(\\))",
                        "There's empty parentheticals in your expression",
                        false),

                new RegexCriterion("(((\\d|\\.)\\()|(\\)(\\d|\\.)))",
                        "You have missed a binary operator somewhere " +
                                "in your expression", false),

                new RegexCriterion("(\\d*\\.\\d*\\.)",
                        "You've been using multiple decimal points in the " +
                                "same number somewhere in your expression",
                        false),

                new RegexCriterion("((\\d+\\.\\d+)|(\\d+\\.)|(\\.\\d+)|(\\d+))",
                        "Your expression does not contain any operands", true),

                new RegexCriterion("((?<!\\d)(\\.)(?!\\d))",
                        "Your expression contains single decimal point with " +
                                "no digits surrounding it", false),

                new RegexCriterion("((?<=(\\)|\\d|\\.))" +
                        "(" + parser.getUnambiguouslyUnaryRegex() + "))",
                        "There's unary being used " +
                                "as binary somewhere in your expression",
                        false),

                new RegexCriterion(parser.getUnambiguouslyBinaryRegex() +
                        "{2,}", "You have more than one binary " +
                        "operator in a row somewhere in your expression.",
                        false),

                new RegexCriterion("((" + parser.getAnyOperatorRegex() +
                        ")(?!(\\d|\\.|\\(|" +
                        parser.getAnySingleUnaryRegex() + ")))",
                        "You have missed an operand " +
                                "somewhere in your expression", false),

                new RegexCriterion("\\s",
                        "Your input consists entirely of whitespace characters",
                        false),

                new Criterion() {
                    public String check(String str) {

                        return (str.length() == 0)
                                ? "Your input is an empty string"
                                : null;
                    }
                },

                new Criterion() {
                    public String check(String str) {

                        return (str.replaceAll("\\(", "").length()
                                != str.replaceAll("\\)", "").length())
                                ? "Your open parentheses count doesn't match " +
                                "your closed parentheses count"
                                : null;

                    }
                },

                new Criterion() {
                    public String check(String str) {

                        String[] unknownTokens = str.replaceAll("(" +
                                parser.getAnyNonOperandRegex() +
                                "|((\\d+\\.\\d+)" +
                                "|(\\d+\\.)" +
                                "|(\\.\\d+)" +
                                "|(\\d+)))", " ")
                                .split("\\s+");

                        if (unknownTokens.length > 0) {

                            StringBuilder sb = new StringBuilder("[");
                            String identifier;

                            int unknownTokensCounter = 0;

                            for (int i = 0; i < unknownTokens.length; i++) {
                                identifier = unknownTokens[i];
                                if (identifier.length() > 0) {
                                    sb.append(identifier);
                                    unknownTokensCounter++;
                                    if (i < unknownTokens.length - 1)
                                        sb.append(", ");
                                }
                            }
                            sb.append("]");
                            boolean isMany = unknownTokensCounter > 1;
                            return String.format("%s %s not valid expression " +
                                            "token%s.", sb.toString(),
                                    (isMany) ? "are" : "is",
                                    (isMany) ? "s" : "");
                        }
                        return null;
                    }
                }

        );

    }
}
