package com.sysgears.calculator.logic;

import com.sysgears.calculator.logic.exceptions.ExpressionSyntaxError;

/**
 * Evaluates string presentation of mathematical expression.
 */
public class Evaluator {

    /**
     * References {@link ScalableParser} instance which is used parse expression string.
     */
    private Parser parser;

    /**
     * Ctor which initialize {@link Evaluator} instance with default.
     *
     * {@link ScalableParser} instance with basic operators registered.
     */
    public Evaluator() {

        this(new ScalableParser());
    }

    /**
     * Ctor which initialize {@link Evaluator} instance with {@link ScalableParser}
     * passed as a parameter.
     *
     * @param parser {@link ScalableParser} instance, used during evaluation
     */
    public Evaluator(final Parser parser) {

        this.parser = parser;
    }

    /**
     * Parses and evaluates expression passed as the parameter.
     *
     * @param expression expression to be evaluated
     * @return expression value
     * @throws ExpressionSyntaxError if there were one or more syntax errors
     * made in expression.
     */
    public double evaluate(final String expression) throws ExpressionSyntaxError {
        String[] infixTokens = parser.tokenizeExpression(expression);
        String[] RPNtokens = parser.infixToRPN(infixTokens);
        Number opTree = parser.RPNToNumberOrTree(RPNtokens);

        return opTree.doubleValue();
    }

}
