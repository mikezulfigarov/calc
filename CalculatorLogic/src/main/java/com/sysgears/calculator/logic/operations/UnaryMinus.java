package com.sysgears.calculator.logic.operations;

/**
 * Represents unary minus operation
 */
public class UnaryMinus extends UnaryOperation {

    /**
     * {@link UnaryMinus} default constructor.
     */
    public UnaryMinus() {

        super(0);
    }

    /**
     * {@link UnaryMinus} constructor. Takes a numeric value a negation of
     * which is expected as a result of the operation
     *
     * @param operand a numeric value a negation of
     *                which is expected as a result
     */
    public UnaryMinus(Number operand) {

        super(operand);
    }

    /**
     * Applies a negation operation to the numeric value passed as a parameter
     *
     * @param operand a numeric value a negation of
     *                which is expected as a result
     * @return negative value of the {@code operand}
     */
    @Override
    public Number apply(Number operand) {

        return -operand.doubleValue();
    }
}
