package com.sysgears.calculator.logic.operations;

import java.lang.reflect.InvocationTargetException;

/**
 * Abstract class encapsulating simple numeric unary operation logic
 */
public abstract class UnaryOperation extends Operation {

    /**
     * The only operand
     */
    public final Number operand;

    /**
     * Default constructor. Sets the {@link UnaryOperation#operand} as zero
     */
    public UnaryOperation() {

        operand = 0;
    }

    /**
     * Constructor. Sets the {@link UnaryOperation#operand}
     *
     * @param operand
     */
    public UnaryOperation(Number operand) {

        this.operand = operand;
    }

    /**
     * Applies this operator to {@link UnaryOperation#operand}.
     * Result depends on your concrete implementation of
     * {@link UnaryOperation#apply(Number)}
     *
     * @return operation result
     */
    @Override
    public Number apply() {

        return apply(operand);
    }

    /**
     * Returns {@link Associativity} type of this operation
     * ( {@link Associativity#RIGHT} by default)
     *
     * @return associativity type of this operation
     * ( {@link Associativity#RIGHT} by default)
     */
    @Override
    public Associativity getAssociativity() {

        return Associativity.RIGHT;
    }

    /**
     * Returns {@link BinaryOperation.Precedence} of this operation
     * ( {@link BinaryOperation.Precedence#HIGHEST} by deafult)
     *
     * @return {@link BinaryOperation.Precedence} of this operation
     * ( {@link BinaryOperation.Precedence#HIGHEST} by deafult)
     */
    @Override
    public BinaryOperation.Precedence getPrecedence() {

        return Precedence.HIGHEST;
    }

    /**
     * Applies actual operator logic to the operand.
     *
     * @param operand operand
     * @return unary operation result for a given operand.
     */
    public abstract Number apply(final Number operand);

    /**
     * Simple builder class for {@link UnaryOperation}'s descendants
     */
    public static class Builder {

        /**
         * Operand field
         */
        private Number operand;

        /**
         * Default ctor. Sets {@link UnaryOperation.Builder#operand} with zero.
         */
        public Builder() {

            this(0);
        }

        /**
         * Parametrized ctor. Sets {@link UnaryOperation.Builder#operand}
         * with the parameter value
         *
         * @param operand value to be set as
         *                {@link UnaryOperation.Builder#operand}
         */
        public Builder(Number operand) {

            setOperand(operand);
        }

        /**
         * Sets {@link UnaryOperation.Builder#operand}
         * with the parameter value
         *
         * @param operand value to be set as
         *                {@link UnaryOperation.Builder#operand}
         * @return this {@link UnaryOperation.Builder} instance
         */
        public Builder setOperand(final Number operand) {

            this.operand = operand;
            return this;
        }

        /**
         * Returns new instance of {@code whichOne.class} set with this
         * {@link Builder}
         *
         * @param whichOne class parameter to specify which class the
         *                 builder should instantiate
         * @return new instance of {@link UnaryOperation} descendant,
         * specifically {@code whichOne.class}
         * @throws NoSuchMethodException     if no appropriate ctor was found
         * @throws IllegalAccessException    if such ctor is out of reach
         *                                   by access
         * @throws InvocationTargetException if there were some errors
         *                                   during the ctor invocation
         * @throws InstantiationException    if a class is abstract
         */
        public UnaryOperation build(Class<? extends UnaryOperation> whichOne)
                throws NoSuchMethodException, IllegalAccessException,
                InvocationTargetException, InstantiationException {

            return whichOne.getConstructor(Number.class)
                    .newInstance(operand);
        }


    }
}
