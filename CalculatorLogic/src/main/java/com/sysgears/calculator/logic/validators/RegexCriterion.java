package com.sysgears.calculator.logic.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * {@link StringValidator.Criterion} implementation for validating
 * {@link String} via regular expressions.
 */
public class RegexCriterion implements StringValidator.Criterion {

    /**
     * The {@link Pattern}, the string being searched for.
     */
    private Pattern pattern;

    /**
     * The message string being returned on validation success (or failure,
     * depends on {@link RegexCriterion#isMsgOnFail} value)
     * by {@link RegexCriterion#check(String)}
     */
    private String msg;

    /**
     * if <b>true</b> - the {@link RegexCriterion#msg} is returned by
     * {@link RegexCriterion#check(String)} on validation <b>failure</b>
     * (matches are not found, or found, but zero-length).
     * <p/>
     * <p/>
     * <p/>
     * <b>Else</b> - the {@link RegexCriterion#msg} is returned by
     * {@link RegexCriterion#check(String)} on validation <b>success</b>
     * (matches are found and not zero-length).
     */
    private boolean isMsgOnFail;

    /**
     * Creates {@link RegexCriterion} instance.
     *
     * @param regex       regular expression to be applied to a string
     * @param message     a message string being returned on validation success
     *                    (or failure, depends on
     *                    {@link RegexCriterion#isMsgOnFail} value) by
     *                    {@link RegexCriterion#check(String)}
     * @param isMsgOnFail if <b>true</b> - the {@link RegexCriterion#msg}
     *                    is returned by {@link RegexCriterion#check(String)}
     *                    on validation <b>failure</b> (matches are not found,
     *                    or found, but zero-length).
     *                    <p/>
     *                    <p/>
     *                    <p/>
     *                    <b>Else</b> - the {@link RegexCriterion#msg} is
     *                    returned by {@link RegexCriterion#check(String)}
     *                    on validation <b>success</b>(matches are found and
     *                    not zero-length).
     */
    public RegexCriterion(final String regex, final String message,
                          final boolean isMsgOnFail) {
        msg = message;
        this.isMsgOnFail = isMsgOnFail;
        pattern = Pattern.compile(regex);
    }

    /**
     * Actually performs validation of the string passed as a parameter.
     * Depending on {@link RegexCriterion#isMsgOnFail} returns either
     * <b>null</b> or {@link RegexCriterion#msg} value, containing validation
     * message.
     *
     * @param str a string to be validated
     * @return depending on {@link RegexCriterion#isMsgOnFail} returns either
     * <b>null</b> or {@link RegexCriterion#msg} value, containing validation
     * message
     */
    public String check(final String str) {

        Matcher matcher = pattern.matcher(str);

        boolean matches = (matcher.find() && (matcher.group(0).length() > 0));

        return (matches != isMsgOnFail) ? msg : null;
    }

}
