package com.sysgears.calculator.logic;

import com.sysgears.calculator.logic.exceptions.ExpressionSyntaxError;

import java.util.Set;

/**
 * Created by mikael-zulfigarov on 17.04.15.
 */
public interface Parser {
    String[] tokenizeExpression(String expression)
            throws ExpressionSyntaxError;

    String[] infixToRPN(String[] inputTokens)
                    throws ExpressionSyntaxError;

    Number RPNToNumberOrTree(String[] tokens);

    Set<String> getAllOperatorsSignatures();

    Set<String> getBinarySignatures();

    Set<String> getUnarySignatures();

    String getUnambiguouslyBinaryRegex();

    String getUnambiguouslyUnaryRegex();

    String getAnyOperatorRegex();

    String getAnySingleUnaryRegex();

    String getAnySingleBinaryRegex();

    String getAnyNonOperandRegex();
}
