package com.sysgears.calculator.logic.exceptions;

/**
 * Checked exception.
 * Thrown when a syntax error in the expression has been detected
 */
public class ExpressionSyntaxError extends Exception {

    /**
     * Constructs exception object with the specified {@code message}
     *
     * @param message string, that contains exception details
     */
    public ExpressionSyntaxError(String message) {

        super(message);
    }

    /**
     * Constructs exception cased by {@code cause} instance.
     *
     * @param cause
     */
    public  ExpressionSyntaxError(Throwable cause) {

        super(cause);
    }
}
