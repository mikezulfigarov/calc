package com.sysgears.calculator.logic;

import com.sysgears.calculator.logic.exceptions.ExpressionSyntaxError;
import com.sysgears.calculator.logic.exceptions.InvalidOperatorSignatureException;
import com.sysgears.calculator.logic.operations.*;
import com.sysgears.calculator.logic.validators.ExpressionValidator;
import com.sysgears.calculator.logic.validators.OperatorValidator;
import com.sysgears.calculator.logic.validators.StringValidator;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Immutable string expression parser.
 */
public final class ScalableParser implements Parser {

    /**
     * Used to configure immutable {@link ScalableParser} instance
     */
    public static class Builder {

        /**
         * Instance to be built
         */
        private ScalableParser instance = null;

        /**
         * Tells built {@link ScalableParser} that {@code stringPresentation} occurence
         * in text should be
         * interpreted as {@code dummy} {@link UnaryOperation}
         *
         * @param stringPresentation String presentation of the
         *                           {@link UnaryOperation}
         * @param dummy              {@link UnaryOperation} object containing
         *                           operation logic.
         */
        public Builder registerUnaryOperator(final String stringPresentation,
                                             final UnaryOperation dummy) {

            lazyInit();

            validateSignature(stringPresentation);

            instance.UNARY_OPERATORS.put(stringPresentation, dummy);

            return this;
        }

        /**
         * Tells {@link ScalableParser} that {@code stringPresentation} occurence in
         * text should be
         * interpreted as {@code dummy} {@link BinaryOperation}
         *
         * @param stringPresentation String presentation of the
         *                           {@link BinaryOperation}
         * @param dummy              {@link BinaryOperation} object containing
         *                           operation logic.
         */
        public Builder registerBinaryOperator(final String stringPresentation,
                                              final BinaryOperation dummy) {

            lazyInit();
            validateSignature(stringPresentation);
            instance.BINARY_OPERATORS.put(stringPresentation, dummy);

            return this;
        }

        /**
         * Returns configured parser instance. If no configuration calls
         * to {@link ScalableParser.Builder
         * #registerBinaryOperator(String, BinaryOperation)}
         * or to {@link ScalableParser.Builder
         * #registerBinaryOperator(String, BinaryOperation)}
         * preceeded then instance with basic binary
         * "+", "-", "/", "*" and unary "-" will be returned
         *
         * @return new configured (if was) or default
         * (if wasn't) {@link ScalableParser} instance.
         */
        public Parser build() {

            lazyInit();
            ScalableParser holder = instance;
            instance = null;
            holder.initOperatorsSubsets();
            holder.expressionValidator = new ExpressionValidator(holder);
            return holder;
        }

        /**
         * Performs lazy initialization of the {@link ScalableParser.Builder#instance}
         */
        private void lazyInit() {
            if (instance == null) instance = new ScalableParser();
        }

        private void validateSignature(String opSign) {

            String error = instance.operatorValidator
                    .getErrorMessagesInOneString(opSign);

            if (error != null) {
                log.fatal(String
                        .format("|[%s] is not a valid method signature.\n|%s",
                                opSign, error));

                throw new InvalidOperatorSignatureException(error);
            }

        }
    }

    /**
     * Regex that matches any positive integer or floating piont value
     */
    private static final String POSITIVE_INT_OR_DOUBLE =
            "((\\d+\\.\\d+)|(\\d+\\.)|(\\.\\d+)|(\\d+))";

    /**
     * Prepend your regex with this if you want the match not to be
     * preceeded by a digit
     */
    private static final String NOT_PRECEDED_BY_OPERAND = "(?<!\\d|\\.|\\))";

    /**
     * Regex that matches space between number and unary minus.
     */
    private static final String SPACE_BETWEEN_NUMBER_AND_UNARY_MINUS =
            "((?<=(?<!((\\)\\s)|(\\d\\s)))(\\-)(?=\\s\\d))\\s)";


    /**
     * Matches one or more whitespace characters
     */
    private static final String ONE_OR_MORE_SPACES = "(\\s+)";

    /**
     * {@link Logger} instance which is used to display parser workflow
     * information in calculator.log
     */
    private static Logger log = Logger.getLogger(ScalableParser.class.getName());

    /**
     * Matches any single registered {@link UnaryOperation} signature
     * on the line
     */
    private String ANY_SINGLE_UNARY;

    /**
     * Matches any single registered {@link BinaryOperation} signature
     * on the line
     */
    private String ANY_SINGLE_BINARY;

    /**
     * Matches any unambiguously (not having same signature defined for
     * {@link BinaryOperation}) registered {@link UnaryOperation} signature
     * on the line. Watch {@link ScalableParser#initOperatorsSubsets()} for definition.
     */
    private String ANY_UNAMBIGUOUSLY_UNARY;

    /**
     * Matches any unambiguously (not having same signature defined for
     * {@link UnaryOperation}) registered {@link BinaryOperation} signature
     * on the line. Watch {@link ScalableParser#initOperatorsSubsets()} for definition.
     * <p/>
     * In general looks like:
     * {@code ((binOperator1)|(binOperator2)|..(binOperatorN))}
     */
    private String ANY_UNAMBIGUOUSLY_BINARY;

    /**
     * Matches any defined operator signature, '(' and ')'.
     * Watch {@link ScalableParser#initOperatorsSubsets()} for definition.
     * <p/>
     * In general looks like:
     * {@code ((operator1)|(operator2)|..(operatorN)|(\\()|(\\)))}
     */
    private String ANY_NON_OPERAND;

    /**
     * Matches any signature from the union of {@link ScalableParser#UNARY_OPERATORS}
     * and {@link ScalableParser#BINARY_OPERATORS} key sets. Watch
     * {@link ScalableParser#initOperatorsSubsets()} for definition.
     * <p/>
     * In general looks like:
     * {@code ((operator1)|(operator2)|..(operatorN))}
     */
    private String ANY_OPERATOR;

    /**
     * Binary operators known to parser
     */
    private final Map<String, BinaryOperation> BINARY_OPERATORS
            = new HashMap<String, BinaryOperation>();

    /**
     * Unary operators known to parser
     */
    private final Map<String, UnaryOperation> UNARY_OPERATORS
            = new HashMap<String, UnaryOperation>();

    /**
     * {@link StringValidator} instance used to validate expression strings
     */
    private ExpressionValidator expressionValidator;

    /**
     * {@link StringValidator} instance used to validate operator signatures
     * during registration
     */
    private OperatorValidator operatorValidator;

    public ScalableParser() {

        BINARY_OPERATORS.put("+", new Sum());
        BINARY_OPERATORS.put("-", new Deduction());
        BINARY_OPERATORS.put("/", new Division());
        BINARY_OPERATORS.put("*", new Multiplication());
        UNARY_OPERATORS.put("-", new UnaryMinus());

        initOperatorsSubsets();
        expressionValidator = new ExpressionValidator(this);
        operatorValidator = new OperatorValidator(this);
    }

    /**
     * Splits raw {@code expression} string into separate infix tokens and
     * returns them as an array of strings
     *
     * @param expression raw {@code expression} string
     * @return an array of string tokens parsed from the given string
     */
    public String[] tokenizeExpression(final String expression)
            throws ExpressionSyntaxError {

        String result = expression.replaceAll(ONE_OR_MORE_SPACES, "");

        MDC.put("expression", result);

        //Try to match malformed expression patterns.
        // If matches not found then moves ahead - else throws an
        // ExpressionSyntaxError
        String errors = expressionValidator.getErrorMessagesInOneString(result);
        if (errors != null) {
            //log.error(errors.replaceAll("\\n",". "));
            throw new ExpressionSyntaxError(errors);
        }

        //applies
        result = applySimpleUnaries(result);

        final String SPACE = " ";

        //Surrounds all non operands with spaces
        result = result.replaceAll(ANY_NON_OPERAND, " $1 ");

        /*Removes all spaces between unary "-"'s (system ones,
         not user defined. They appear if there were negative results
         of a simple unary operators application).
         Spaces in this case are delimiters for tokens as a e.g. "-5.0"
         is a single token*/
        result = result.replaceAll(SPACE_BETWEEN_NUMBER_AND_UNARY_MINUS, "");

        //Removes redundant spaces
        result = result.replaceAll(ONE_OR_MORE_SPACES, SPACE);

        /*Surrounds all sequences of unary operators or spaces
         preceding opening "(" with "[ ]".
         It is used during infix to RPN conversion
         and further expression evaluation
         for correct application of the unary
         operators sequence to the corresponding parentheticals.*/
        result = result.replaceAll("(((?<!(\\d|\\)|\\.)\\s)((" +
                ANY_SINGLE_UNARY + "\\s?)+)(?=(\\s\\()|(" +
                ANY_SINGLE_UNARY + "\\s?)))|((?<=(((\\d|\\)|\\.)\\s)" +
                ANY_SINGLE_BINARY + "))(\\s+)(?=\\()))", " [ $1 ] ");

        /*Trims the beginning/ending spaces
        and returns as the array of tokens split by one or more spaces.*/

        String[] resultTokenized = result.trim().split(ONE_OR_MORE_SPACES);

        log.debug(String.format("Has been tokenized as %s", Arrays.toString(resultTokenized).replaceAll("\\,","")));

        return resultTokenized;
    }

    /**
     * Shunting-yard algorithm implementation for converting array
     * of infix notation string tokens to array of RPN string tokens
     *
     * @param inputTokens infix notation tokens array
     * @return RPN tokens array
     */
    public String[] infixToRPN(final String[] inputTokens)
            throws ExpressionSyntaxError {

        ArrayList<String> out = new ArrayList<String>();
        Stack<String> stack = new Stack<String>();
        Stack<Stack<String>> unOpStack = new Stack<Stack<String>>();
        boolean isInUnaryRange = false;

        for (String token : inputTokens) {
            if (!isInUnaryRange) {
                if (isOperator(token)) {
                    while (!stack.empty() && isOperator(stack.peek())) {
                        if ((isAssociative(token, Operation.Associativity.LEFT)
                                && cmpPrec(token, stack.peek()) <= 0)
                                || (isAssociative(token,
                                Operation.Associativity.RIGHT)
                                && cmpPrec(token, stack.peek()) < 0)) {
                            out.add(stack.pop());
                            continue;
                        }
                        break;
                    }
                    stack.push(token);
                } else if (isOpeningParenthesis(token)) {
                    stack.push(token);
                } else if (token.equals("[")) {
                    isInUnaryRange = true;
                    unOpStack.push(new Stack<String>()).push("]");
                } else if (isClosingParenthesis(token)) {
                    while (!stack.empty() &&
                            !isOpeningParenthesis(stack.peek()))
                        out.add(stack.pop());
                    if (!unOpStack.empty()) {
                        while (!unOpStack.peek().empty()) {
                            out.add(unOpStack.peek().pop());
                        }
                        unOpStack.pop();
                    }
                    stack.pop();
                } else {
                    out.add(token);
                }
            } else {
                if (token.equals("]")) {
                    unOpStack.peek().push("[");
                    isInUnaryRange = false;
                } else if (isUnary(token)) unOpStack.peek().push(token);
                else
                    throw new ExpressionSyntaxError("Looks like you're using " +
                            "binary operator as an unary for parenthetical " +
                            "expression. It's not okay.");
            }
        }
        while (!stack.empty()) {
            out.add(stack.pop());
        }

        String[] output = new String[out.size()];
        output = out.toArray(output);
        log.debug(String.format("Has been transformed to RPN as %s",
                Arrays.toString(output).replaceAll("\\,","")));

        return output;
    }

    /**
     * Converts RPN to operation tree.
     *
     * @param tokens Reverse Polish notation tokens
     * @return {@link Number} result of evaluation. Could be checked
     * if instance of {@link BinaryOperation} type. If so, could be traversed
     * as a binary operation tree (or can just call
     * {@link BinaryOperation#apply()} to evaluate it).You can just
     */
    public Number RPNToNumberOrTree(final String[] tokens) {

        Stack<Number> resultStack = new Stack<Number>();
        boolean isApplyingUnaries = false;

        for (String token : tokens) {
            if (!isApplyingUnaries) {
                if (token.equals("[")) {
                    isApplyingUnaries = true;
                } else if (!isOperator(token)) {
                    resultStack.push(Double.valueOf(token));
                } else {
                    Number rightOp = resultStack.pop();
                    Number result;
                    if (!resultStack.empty() && isBinary(token)) {
                        Number leftOp = resultStack.pop();
                        result = BINARY_OPERATORS.get(token)
                                .apply(leftOp, rightOp);
                    } else {
                        result = UNARY_OPERATORS.get(token).apply(rightOp);
                    }
                    resultStack.push(result);
                }
            } else {
                if (!token.equals("]")) {
                    resultStack.push(UNARY_OPERATORS.get(token)
                            .apply(resultStack.pop()));
                } else {
                    isApplyingUnaries = false;
                }
            }
        }

        return resultStack.pop();
    }

    /**
     * Returns the {@link ScalableParser#union(Set, Set, String...)} of registered
     * {@link UnaryOperation}s and {@link BinaryOperation}s signatures
     *
     * @return the {@link ScalableParser#union(Set, Set, String...)} of registered
     * {@link UnaryOperation}s and {@link BinaryOperation}s signatures
     */
    public Set<String> getAllOperatorsSignatures() {

        return union(BINARY_OPERATORS.keySet(), UNARY_OPERATORS.keySet());
    }

    /**
     * Returns the {@link Set} of registered {@link BinaryOperation}s
     * signatures
     *
     * @return the {@link Set} of registered {@link BinaryOperation}s
     * signatures
     */
    public Set<String> getBinarySignatures() {

        return BINARY_OPERATORS.keySet();
    }

    /**
     * Returns the {@link Set} of registered {@link UnaryOperation}s
     * signatures
     *
     * @return the {@link Set} of registered {@link UnaryOperation}s
     * signatures
     */
    public Set<String> getUnarySignatures() {

        return UNARY_OPERATORS.keySet();
    }

    /**
     * Getter for {@link ScalableParser#ANY_UNAMBIGUOUSLY_BINARY}
     *
     * @return {@link ScalableParser#ANY_UNAMBIGUOUSLY_BINARY} value
     */
    public String getUnambiguouslyBinaryRegex() {

        return ANY_UNAMBIGUOUSLY_BINARY;
    }

    /**
     * Getter for {@link ScalableParser#ANY_UNAMBIGUOUSLY_UNARY}
     *
     * @return {@link ScalableParser#ANY_UNAMBIGUOUSLY_UNARY} value
     */
    public String getUnambiguouslyUnaryRegex() {

        return ANY_UNAMBIGUOUSLY_UNARY;
    }

    public String getAnyOperatorRegex() {

        return ANY_OPERATOR;
    }

    /**
     * Getter for {@link ScalableParser#ANY_SINGLE_UNARY}
     *
     * @return {@link ScalableParser#ANY_SINGLE_UNARY} value
     */
    public String getAnySingleUnaryRegex() {

        return ANY_SINGLE_UNARY;
    }

    /**
     * Getter for {@link ScalableParser#ANY_SINGLE_BINARY}
     *
     * @return {@link ScalableParser#ANY_SINGLE_BINARY} value
     */
    public String getAnySingleBinaryRegex() {

        return ANY_SINGLE_BINARY;
    }

    /**
     * Getter for {@link ScalableParser#ANY_NON_OPERAND}
     *
     * @return {@link ScalableParser#ANY_NON_OPERAND} value
     */
    public String getAnyNonOperandRegex() {

        return ANY_NON_OPERAND;
    }

    /**
     * Initializes operators regexes that matches specific subsets
     * of registered operators
     */
    private void initOperatorsSubsets() {

        ANY_SINGLE_UNARY = generateRegexForSet(UNARY_OPERATORS.keySet());

        ANY_SINGLE_BINARY = generateRegexForSet(BINARY_OPERATORS.keySet());

        ANY_UNAMBIGUOUSLY_UNARY = generateRegexForSet(
                substract(UNARY_OPERATORS.keySet(),
                        BINARY_OPERATORS.keySet()));

        Set<String> unambiguouslyUnary = substract(BINARY_OPERATORS.keySet(),
                UNARY_OPERATORS.keySet());

        ANY_UNAMBIGUOUSLY_BINARY = generateRegexForSet(unambiguouslyUnary);

        ANY_NON_OPERAND = generateRegexForSet(union(UNARY_OPERATORS.keySet(),
                BINARY_OPERATORS.keySet(), "(", ")"));

        ANY_OPERATOR = generateRegexForSet(union(UNARY_OPERATORS.keySet(),
                BINARY_OPERATORS.keySet()));
    }

    /**
     * Checks whether {@code token} is a registered operator
     *
     * @param token checked token
     * @return <b>true</b> - if token is registered operator,
     * else - <b>false</b>
     */
    private boolean isOperator(final String token) {

        return isBinary(token) || isUnary(token);
    }

    /**
     * Checks token associativity
     *
     * @param token checked token
     * @param type  expected {@link Operation.Associativity}
     * @return whether the token has the expected
     * {@link Operation.Associativity}
     */
    private boolean isAssociative(final String token,
                                  final BinaryOperation.Associativity type) {

        if (!isOperator(token)) {
            throw new IllegalArgumentException("Invalid token: " + token);
        }

        BinaryOperation.Associativity opAssoc = (isBinary(token)) ?
                BINARY_OPERATORS.get(token).getAssociativity() :
                Operation.Associativity.LEFT;

        return opAssoc == type;
    }

    /**
     * Applies a sequence of unary operators to a value, prepended by them.
     *
     * @param opTok  operator tokens. applied to {@code number}
     * @param number a value, which operands are applied to
     * @return result of an unary operation sequence application
     */
    private String processSingleUnary(final String[] opTok,
                                      final String number) {

        Double result = Double.valueOf(number);

        for (int i = opTok.length - 1; i >= 0; i--) {
            result = UNARY_OPERATORS.get(opTok[i]).apply(result).doubleValue();
        }

        return " " + result.toString() + " ";
    }

    /**
     * Transforms raw string containing unary operators to an array of token
     * strings.
     *
     * @param opSeq unary operator sequence as a raw string
     * @return array of tokenized unary operators
     */
    private String[] tokenizeUnaries(final String opSeq) {

        String result = opSeq;

        result = result.replaceAll(ANY_SINGLE_UNARY, " $1 ");

        return result.trim().split(ONE_OR_MORE_SPACES);
    }

    /**
     * Returns new {@link Set} which is substract of
     * {@link Set}s passed as the parameters
     *
     * @param setL {@link Set} to be substracted from
     * @param setR {@link Set} to be substracted
     * @return ew {@link Set} which is substract of
     * {@link Set}s passed as the parameters
     */
    private Set<String> substract(final Set<String> setL,
                                  final Set<String> setR) {

        Set<String> result = new HashSet<String>(setL);
        result.removeAll(setR);
        return result;
    }

    /**
     * Returns new {@link Set} which is result of the union operation of
     * {@link Set}s passed as the parameters
     *
     * @param setL {@link Set}
     * @param setR {@link Set}
     * @return union of {@code setL} and {@code setR} as {@link Set}
     */
    private Set<String> union(final Set<String> setL, final Set<String> setR,
                              final String... appendix) {

        Set<String> result = new HashSet<String>();
        result.addAll(setL);
        result.addAll(setR);
        result.addAll(Arrays.asList(appendix));
        return result;
    }

    /**
     * Searches {@link UnaryOperation}s with operands that are not
     * parentheticals and replace matches with it's application results.
     *
     * @param expression expression to be processed
     * @return {@code expression} with partially processed unary operators.
     */
    private String applySimpleUnaries(final String expression) {

        String SINGLE_KNOWN_UNARY = generateRegexForSet(UNARY_OPERATORS
                .keySet());
        String UNARY_SEQUENCE = "(" + SINGLE_KNOWN_UNARY + "+)";

        Matcher evaluableUnaries = Pattern.compile(NOT_PRECEDED_BY_OPERAND +
                UNARY_SEQUENCE +
                POSITIVE_INT_OR_DOUBLE).matcher(expression);

        StringBuffer sbuf = new StringBuffer();
        String opSeq, operand = "";

        while (evaluableUnaries.find()) {
            opSeq = evaluableUnaries.group(1);
            for (int i = 1; i <= evaluableUnaries.groupCount(); i++) {
                operand = evaluableUnaries.group(i);
                if ((operand != null)
                        && operand.matches(POSITIVE_INT_OR_DOUBLE)) {
                    break;
                }
            }
            evaluableUnaries.appendReplacement(sbuf,
                    processSingleUnary(tokenizeUnaries(opSeq), operand));
        }
        return evaluableUnaries.appendTail(sbuf).toString();
    }

    /**
     * Generates regex which matches every single occurence of set element in a
     * string
     *
     * @param set set of operator signatures
     * @return regex which matches every single occurence of set element in a
     * string
     */
    private String generateRegexForSet(final Set<String> set) {

        StringBuilder sb = new StringBuilder("(");
        for (String op : set) {
            sb.append(op.matches("\\w+") ? "(" : "(\\")
                    .append(op)
                    .append(")|");
        }
        if (set.size() > 0)
            sb.deleteCharAt(sb.length() - 1);
        sb.append(")");
        return sb.toString();
    }

    /**
     * Compares operators represented by string by precedence
     *
     * @param token1 first comparant
     * @param token2 second comparant
     * @return <b>(result > 0)</b> if this {@link Operation} has higher
     * {@link BinaryOperation.Precedence}.<br/>
     * <b>(result == 0) </b> if equal.<br/>
     * <b>(result < 0) </b> if lower.<br/>
     */
    private int cmpPrec(final String token1, final String token2) {

        return BINARY_OPERATORS.get(token1)
                .compareTo(BINARY_OPERATORS.get(token2));
    }

    /**
     * Returns whether operator represented by token has been defined as
     * {@link UnaryOperation}
     *
     * @param token string representation of an operator
     * @return whether operator represented by token has been defined as
     * {@link UnaryOperation}
     */
    private boolean isUnary(final String token) {

        return UNARY_OPERATORS.containsKey(token);
    }

    /**
     * Returns whether operator represented by token has been defined as
     * {@link BinaryOperation}
     *
     * @param token string representation of an operator
     * @return whether operator represented by token has been defined as
     * {@link BinaryOperation}
     */
    private boolean isBinary(final String token) {

        return BINARY_OPERATORS.containsKey(token);
    }

    /**
     * Returns whether string token passed as to a method is equal to
     * opening parenthesis
     *
     * @param token string token
     * @return whether string token passed as to a method is equal to
     * opening parenthesis
     */
    private boolean isOpeningParenthesis(final String token) {

        return token.equals("(");
    }

    /**
     * Returns whether string token passed as to a method is equal to
     * closing parenthesis
     *
     * @param token string token
     * @return whether string token passed as to a method is equal to
     * opening parenthesis
     */
    private boolean isClosingParenthesis(final String token) {

        return token.equals(")");
    }

}
