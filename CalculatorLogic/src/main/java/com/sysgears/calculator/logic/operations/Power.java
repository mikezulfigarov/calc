package com.sysgears.calculator.logic.operations;

/**
 * Represents power operation
 */
public class Power extends BinaryOperation {

    /**
     * {@link Sum} default constructor
     */
    public Power() {

        super(0, 0);
    }

    /**
     * {@link Power} constructor, which specifies it's state
     *
     * @param leftOp  left operand of the {@link Power}
     * @param rightOp right operand of the {@link Power}
     */
    public Power(Number leftOp, Number rightOp) {

        super(leftOp, rightOp);
    }

    /**
     * Applies power operation to the given {@code leftOp}
     * and {@code rightOp} operands
     *
     * @param leftOp  left operand
     * @param rightOp right operand
     * @return a {@code leftOp} in the power of {@code rightOp}
     */
    @Override
    public Number apply(Number leftOp, Number rightOp) {

        return Math.pow(leftOp.doubleValue(), rightOp.doubleValue());
    }

    /**
     * Returns {@link Associativity} type of this operation
     * ( {@link Associativity#LEFT} by default)
     *
     * @return associativity type of this operation
     * ( {@link Associativity#LEFT} by default)
     */
    @Override
    public Associativity getAssociativity() {

        return Associativity.LEFT;
    }

    /**
     * Returns {@link Precedence} of this operation
     * ( {@link Precedence#HIGHEST} by deafult)
     *
     * @return {@link Precedence} of this operation
     * ( {@link Precedence#HIGHEST} by deafult)
     */
    @Override
    public Precedence getPrecedence() {

        return Precedence.HIGHEST;
    }
}
