import static org.testng.Assert.*;

import com.sysgears.calculator.logic.ScalableParser;
import com.sysgears.calculator.logic.validators.ExpressionValidator;
import org.testng.annotations.Test;

public class ValidatorTests {

    /**
     * Tests empty parentheses validation
     */
    @Test
    public void emptyParenthesesTest() {
        assertTrue(new ExpressionValidator(new ScalableParser())
                .getErrorMessagesInOneString("()")
                .contains("There's empty parentheticals in your expression"));
    }

    /**
     * Tests no operands validation
     */
    @Test
    public void noOperandsTest() {
        assertTrue(new ExpressionValidator(new ScalableParser())
                .getErrorMessagesInOneString("+")
                .contains("Your expression does not contain any operands"));
    }

    @Test
    public void multipleDecimalPointsTest() {
        assertTrue(new ExpressionValidator(new ScalableParser())
                .getErrorMessagesInOneString("2..2")
                .contains("You've been using multiple decimal points in the " +
                        "same number somewhere in your expression"));
    }

    @Test
    public void siglePointTest() {
        assertTrue(new ExpressionValidator(new ScalableParser())
                .getErrorMessagesInOneString(".")
                .contains("Your expression contains single decimal point with " +
                        "no digits surrounding it"));
    }

    @Test
    public void missedBinaryTest() {
        assertTrue(new ExpressionValidator(new ScalableParser())
                .getErrorMessagesInOneString("2(")
                .contains("You have missed a binary operator somewhere " +
                        "in your expression"));
    }

}
